const axios = require("axios")
const express = require("express")
const app = express()
const { PORT = 3000 } = process.env

const GOOGLE_CLIENT_ID = "635849900942-ehhqetk3iv9ft5sgl8qmu1vplfcr157m.apps.googleusercontent.com"
const GOOGLE_CLIENT_SECRET = "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"
const GOOGLE_REDIRECT_URI = "http://localhost:3000/"
const GOOGLE_SCOPE = "https://www.googleapis.com/auth/userinfo.profile openid"

app.get("/login", (req, res) => {
    const uris = [
        `https://accounts.google.com/o/oauth2/v2/auth?client_id=${GOOGLE_CLIENT_ID}`,
        `response_type=code`,
        `access_type=offline`,
        `scope=${encodeURIComponent(GOOGLE_SCOPE)}`,
        `redirect_uri=${GOOGLE_REDIRECT_URI}`,
    ]
    res.redirect(uris.join("&"))
})

app.get("/", async (req, res) => {
    if (req.query.code) {
        let data = {}
        const forms = [
            `code=${req.query.code}`,
            `client_id=${GOOGLE_CLIENT_ID}`,
            `client_secret=${GOOGLE_CLIENT_SECRET}`,
            `grant_type=authorization_code`,
            `redirect_uri=${GOOGLE_REDIRECT_URI}`,
        ]
        try {
            let accessToken = {}
            let rawResponse = await axios.post("https://oauth2.googleapis.com/token", forms.join("&"), {
                headers: {
                    "content-type": "application/x-www-form-urlencoded",
                    "accept": "application/json",
                }
            })
            accessToken = rawResponse.data
            let rawUserResponse = await axios.get("https://www.googleapis.com/oauth2/v1/userinfo?alt=json", {
                headers: {
                    "authorization": [accessToken.token_type, accessToken.access_token].join(" "),
                    "accept": "application/json",
                }
            })
            data = rawUserResponse.data
        } catch (e) {
            if (e.response && e.response.data) {
                data.error = e.response.data
            } else {
                data.error = e.message
            }
        }
        return res.json(data)
    }

    res.redirect("/login")
})

app.listen(PORT, () => {
    console.log(`express google oauth2 running on port ${PORT}`)
})