const Testimonial = () => {
    return (
        <>
        <div id="testimonial" className="testimonial">
            <h3>Testimonial</h3>
            <p style={{textAlign: 'center', marginTop: 16, fontSize: 14}}>Berbagai review positif dari pelanggan kami</p>
        </div>

        <div id="owl-container">
            <div className="owl-carousel owl-theme">
                <div className="item">
                    <figure>
                        <div className="row owl-row">
                            <div className="col-lg-3 col-md-3">
                                <img className="testi-profile" src="assets/images/img_photo.png" alt="Testi Profile"/>
                            </div>
                            <div className="col-lg-6 col-md-6">

                                <img className="star-rate" src="assets/images/Rate.svg" alt="Rate"/>
                                <p className="testi-text ">"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus."</p>
                                <p className="testi-name">John Dee 32, Bromo</p>
                            </div>
                        </div>

                    </figure>
                </div>
                <div className="item">
                    <figure>
                        <div className="row owl-row">
                            <div className="col-lg-3 col-md-3">
                                <img className="testi-profile" src="assets/images/img_photo.png" alt="Testi Profile"/>
                            </div>
                            <div className="col-lg-6 col-md-6">

                                <img className="star-rate" src="assets/images/Rate.svg" alt="Rate"/>
                                <p className="testi-text ">"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus."</p>
                                <p className="testi-name">John Dee 32, Bromo</p>
                            </div>
                        </div>

                    </figure>
                </div>
                <div className="item">
                    <figure>
                        <div className="row owl-row">
                            <div className="col-lg-3 col-md-3">
                                <img className="testi-profile" src="assets/images/img_photo.png" alt="Testi Profile"/>
                            </div>
                            <div className="col-lg-6 col-md-6">

                                <img className="star-rate" src="assets/images/Rate.svg" alt="Rate"/>
                                <p className="testi-text ">"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus."</p>
                                <p className="testi-name">John Dee 32, Bromo</p>
                            </div>
                        </div>

                    </figure>
                </div>
                <div className="item">
                    <figure>
                        <div className="row owl-row">
                            <div className="col-lg-3 col-md-3">
                                <img className="testi-profile" src="/assets/images/img_photo.png" alt="Testi Profile"/>
                            </div>
                            <div className="col-lg-6 col-md-6">

                                <img className="star-rate" src="/assets/images/Rate.svg" alt="Rate"/>
                                <p className="testi-text ">"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus."</p>
                                <p className="testi-name">John Dee 32, Bromo</p>
                            </div>
                        </div>

                    </figure>
                </div>
            </div>
        </div>
        </>
    )
}

export default Testimonial