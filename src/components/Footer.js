const Footer = () => {
    return (
        <div className="w-100 py-4 flex-shrink-0 ">
        <div className="container py-4 ">
            <div className="row gy-4 gx-5 ">
                <div className="col-lg-4 col-md-6 ">
                    <p className="small ">Jalan Suroyo No. 161 Mayangan Kota <br/>Probolinggo 672000</p>
                    <p className="small ">binarcarrental@gmail.com</p>
                    <p className="small ">081-233-334-808</p>
                </div>
                <div className="col-lg-2 col-md-6 ">
                    <p className="small "><a href="/" style={{color: 'black', fontWeight: 'bold'}}>Our Service</a></p>
                    <p className="small "><a href="/" style={{color: 'black', fontWeight: 'bold'}}>Why Us</a></p>
                    <p className="small "><a href="/" style={{color: 'black', fontWeight: 'bold'}}>Testimonial</a></p>
                    <p className="small "><a href="/" style={{color: 'black', fontWeight: 'bold'}}>FAQ</a></p>
                </div>
                <div className="col-lg-4 col-md-6 ">
                    <p className="small ">Connect with us</p>
                    <div className="social-icon ">
                        <a href="/"><i className="fa "><img src="assets/images/facebook.svg " alt="Facebook "/></i></a>
                        <a href="/"><i className="fa "><img src="assets/images/fi_instagram.svg " alt="Instagram "/></i></a>
                        <a href="/"><i className="fa "><img src="assets/images/fi_twitter.svg " alt="Twitter "/></i></a>
                        <a href="/"><i className="fa "><img src="assets/images/fi_mail.svg " alt="Mail "/></i></a>
                        <a href="/"><i className="fa "><img src="assets/images/fi_twitch.svg " alt="Twitch "/></i></a>
                    </div>

                </div>
                <div className="col-lg-2 col-md-6 ">
                    <p className="small ">Copyright Binar 2022.</p>
                    <a className="footer-brand " href="/">&nbsp;</a>
                </div>
            </div>
        </div>
    </div>
    )
}

export default Footer