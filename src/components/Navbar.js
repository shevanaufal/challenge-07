const Navbar = () => {
  return (
    <nav className="navbar navbar-expand-lg navbar-light fixed-top">
      <a className="navbar-brand" href="/">&nbsp;</a>
      <button
        className="navbar-toggler ml-auto"
        type="button"
        data-toggle="collapse"
        data-target="#navbarNav"
        aria-controls="navbarNav"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="nav navbar-nav ml-auto">
          <li className="d-lg-none">
            BCR
            <a className="navbar-nav-toggler" href="/">
              &times;
            </a>
          </li>
          <li className="nav-item active">
            <a className="nav-link" href="/">
              Our Service <span className="sr-only">(current)</span>
            </a>
          </li>
          <li className="nav-item active">
            <a className="nav-link" href="/">
              Why Us
            </a>
          </li>
          <li className="nav-item active">
            <a className="nav-link active" href="/">
              Testimonial
            </a>
          </li>
          <li className="nav-item active">
            <a className="nav-link" href="/">
              FAQ
            </a>
          </li>
          <li className="nav-item active">
            <a className="btn navbar-btn " href="/">
              Register
            </a>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default Navbar;
