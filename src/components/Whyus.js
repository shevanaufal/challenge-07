const Whyus = () => {
    return (
        <>
        <div id="why_us" className="whyus">
            <h3>Why Us</h3>
            <p style={{marginLeft: 136, marginTop: 16}}>Mengapa harus pilih Binar Car Rental?</p>
            <div className="container">
                <div className="row" style={{marginLeft: -32}}>
                    <div className="col-lg-3 col-md-3">
                        <div className="card" style={{width: 275, height: 200}}>
                            <div className="card-body">
                                <img src="assets/images/icon_thumbs.svg" alt="Thumbs SVG" 
                                style={{
                                    position: 'static', 
                                    width: 32, 
                                    height: 32, 
                                    left: 24, 
                                    top: 24}}/>
                                <h5 className="card-title">Mobil Lengkap</h5>
                                <p className="card-text">Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat.</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-3">
                        <div className="card" style={{width: 275, height: 200}}>
                            <div className="card-body" >
                                <img src="assets/images/icon_price.svg" alt="Price SVG" style={{
                                    position: 'static', 
                                    width: 32, 
                                    height: 32, 
                                    left: 24, 
                                    top: 24}} />
                                <h5 className="card-title">Harga Murah</h5>
                                <p className="card-text">Harga murah dan bersaing, bisa dibandingkan harga kami dengan rental mobil lain.</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-3">
                        <div className="card" style={{width: 275, height: 200}}>
                            <div className="card-body">
                                <img src="assets/images/icon_24hrs.svg" alt="24hrs SVG" style={{
                                    position: 'static', 
                                    width: 32, 
                                    height: 32, 
                                    left: 24, 
                                    top: 24}}/>
                                <h5 className="card-title">Layanan 24 Jam</h5>
                                <p className="card-text">Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu.</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-3">
                        <div className="card" style={{width: 275, height: 200}}>
                            <div className="card-body">
                                <img src="assets/images/icon_professional.svg" alt="Profesional SVG" style={{
                                    position: 'static', 
                                    width: 32, 
                                    height: 32, 
                                    left: 24, 
                                    top: 24}}/>
                                <h5 className="card-title">Sopir Profesional</h5>
                                <p className="card-text">Sopir yang profesional, berpengalaman, jujur, ramah, dan selalu tepat waktu.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}

export default Whyus