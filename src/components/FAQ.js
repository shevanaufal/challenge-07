const FAQ = () => {
    return (
        <>
        <div id="faq" className="faq">
            <div className="container ">
                <div className="row">
                    <div className="col-md-4 ">
                        <h3 className="faq-title">Frequently Asked Question</h3>
                        <p className="faq-text" style={{textAlign: 'left', marginTop: 16}}>Berbagai review positif dari pelanggan kami</p>

                    </div>
                    <div className="col-md-2"></div>
                    <div className="col-md-6">
                        <div className="accordion" id="accordionExample">
                            <div className="card-accordion">
                                <div className="card-header collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                    <span className="title">Apa saja syarat yang dibutuhkan? </span>
                                    <span className="accicon"><i className="fas fa-angle-down rotate-icon "></i></span>
                                </div>
                                <div id="collapseOne" className="collapse show" data-parent="#accordionExample">
                                    <div className="card-body">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                                        in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                    </div>
                                </div>
                            </div>
                            <div className="card-accordion">
                                <div className="card-header collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <span className="title">Berapa hari minimal sewa mobil lepas kunci?</span>
                                    <span className="accicon "><i className="fas fa-angle-down rotate-icon "></i></span>
                                </div>
                                <div id="collapseTwo" className="collapse" data-parent="#accordionExample">
                                    <div className="card-body ">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                                        in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                    </div>
                                </div>
                            </div>
                            <div className="card-accordion">
                                <div className="card-header collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false">
                                    <span className="title">Berapa hari sebelumnya sebaiknya booking sewa mobil?</span>
                                    <span className="accicon"><i className="fas fa-angle-down rotate-icon"></i></span>
                                </div>
                                <div id="collapseThree" className="collapse" data-parent="#accordionExample">
                                    <div className="card-body">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                                        in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                    </div>
                                </div>
                            </div>
                            <div className="card-accordion">
                                <div className="card-header collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false">
                                    <span className="title">Apakah ada biaya antar-jemput?</span>
                                    <span className="accicon"><i className="fas fa-angle-down rotate-icon"></i></span>
                                </div>
                                <div id="collapseFour" className="collapse" data-parent="#accordionExample">
                                    <div className="card-body ">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                                        in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                    </div>
                                </div>
                            </div>
                            <div className="card-accordion">
                                <div className="card-header collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false">
                                    <span className="title ">Bagaimana jika terjadi kecelakaan?</span>
                                    <span className="accicon "><i className="fas fa-angle-down rotate-icon"></i></span>
                                </div>
                                <div id="collapseFive" className="collapse" data-parent="#accordionExample">
                                    <div className="card-body">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                                        in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}

export default FAQ