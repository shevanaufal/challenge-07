const Ourservice = () => {
    return (
        <>
        <div id="our_service" className="jumbotron2">
            <div className="row">
                <div className="col-md-6">
                    <img className="img_service" src="assets/images/img_service.png" alt="service"/>
                </div>
                <div className="col-md-6">
                    <div className="content_service">
                        <h2 className="display-4">Best Car Rental for any kind of trip in <br/> (Lokasimu)!</h2>
                        <p className="lead">Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain, kondisi mobil baru, serta kualitas <br/>pelayanan terbaikuntuk perjalanan wisata, bisnis , wedding, meeting, dll.</p>
                        <ul className="list-group">
                            <li className="list-group-item border-0"><img src="assets/images/check.png" style={{
                                position: 'static', width: 24, height: 24, left: 0, top: 0, marginRight: 16}} alt="check"/> Sewa Mobil Lepas Kunci di Bali 24 jam</li>
                            <li className="list-group-item border-0"><img src="assets/images/check.png" style={{
                                position: 'static', width: 24, height: 24, left: 0, top: 0, marginRight: 16}} alt="check"/> Sewa Mobil Jangka Panjang Bulanan</li>
                            <li className="list-group-item border-0"><img src="assets/images/check.png" style={{
                                position: 'static', width: 24, height: 24, left: 0, top: 0, marginRight: 16}} alt="check"/> Gratis Antar - Jemput Mobil di Bandara</li>
                            <li className="list-group-item border-0"><img src="assets/images/check.png" style={{
                                position: 'static', width: 24, height: 24, left: 0, top: 0, marginRight: 16}} alt="check"/> Layanan Airport Transfer / Drop In Out</li>
                            <li className="list-group-item border-0"><img src="assets/images/check.png" style={{
                                position: 'static', width: 24, height: 24, left: 0, top: 0, marginRight: 16}} alt="check"/> Sewa Mobil Dengan Supir di Bali 12 jam</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        </>
    )
}

export default Ourservice