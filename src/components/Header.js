const Header = () => {
  return (
    <div id="navbar-brand" className="jumbotron">
      <div className="row">
        <div className="col-md-6">
          <h1 className="display-4">
            Sewa and Rental Mobil Terbaik di kawasan (Lokasimu)
          </h1>
          <p className="lead">
            Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas
            terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu
            untuk sewa mobil selama 24 jam.
          </p>
          <a className="btn btn-success jumbotron-btn" href="cars">
            Mulai Sewa Mobil
          </a>
        </div>
        <div className="col-md-6"></div>
      </div>
    </div>
  );
};

export default Header;
