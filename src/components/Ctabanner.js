const Ctabanner = () => {
    return (
        <>
        <div className=" cta-banner ">
            <div className="container ">
                <div className="row ">
                    <div className="col-md-auto ">
                        <div className="card-cta text-center ">
                            <div className="card-body2 ">
                                <h5 className="card-title-cta ">Sewa Mobil di (Lokasimu) Sekarang</h5>
                                <p className="card-text-cta ">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis.</p>
                                <a href="/" className="btn btn-success cta-btn ">Mulai Sewa Mobil</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}

export default Ctabanner