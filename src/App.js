// import logo from './logo.svg';
// import './App.css';

import { BrowserRouter, Route, Routes } from "react-router-dom";
import Footer from "./components/Footer";
import Header from "./components/Header";
import Navbar from "./components/Navbar";
import Home from "./pages/Home";
import Cars from "./pages/Cars";

// import { 
//   GoogleLogin, 
//   GoogleOAuthProvider,
//   googleLogout
// } from '@react-oauth/google';

// const GOOGLE_OAUTH2_CLIENT_ID="635849900942-ehhqetk3iv9ft5sgl8qmu1vplfcr157m.apps.googleusercontent.com"

function App() {

  return (
    <BrowserRouter>
        <Navbar/>
        <Header/>
        <Routes>
            <Route path="/" element={<Home/>} />
            <Route path="/cars" element={<Cars/>} />
        </Routes>
        <Footer/>
    </BrowserRouter>
  );
}

export default App;
