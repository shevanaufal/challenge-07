import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';
// import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import store from "./app/store";
// Import the functions you need from the SDKs you need
// import { initializeApp } from "firebase/app";
// import { getAnalytics } from "firebase/analytics";

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>
);


// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
// const firebaseConfig = {
//   apiKey: "AIzaSyDQNnr5RL207CwNxo5HllWz7zyYjcvLKbs",
//   authDomain: "react-app-a33ec.firebaseapp.com",
//   projectId: "react-app-a33ec",
//   storageBucket: "react-app-a33ec.appspot.com",
//   messagingSenderId: "635849900942",
//   appId: "1:635849900942:web:1966d71ce836164163dadf",
//   measurementId: "G-QP7ZG504SN"
// };

// Initialize Firebase
// const app = initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
