import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAsyncData } from "../reducers/api-store";
import Car from "../components/Car"

const Cars = () => {
  const [driver, setDriver] = useState("")
  const dispatch = useDispatch()
  const listCarsJson = useSelector((state) => state.api.cars);

  useEffect(() => {
    dispatch(getAsyncData())
  }, [dispatch])

  const showBackdrop = () => {
    document.querySelector(".modal-backdrop").style.display = "block"
    document.body.classList.add("modal-open")
  }

  const hideBackdrop = (e) => {
    document.querySelector(".modal-backdrop").style.display = "none"
    document.body.classList.remove("modal-open");
    e.target.blur()
  }
  const onSearch = () => {
    //onClick={onSearch}
    console.log(driver)
    console.log(listCarsJson)
  }

  return (
    <>
      <div className="container mt-4">
        <div id="filter-box">
          <div className="card mb-4" style={{ marginLeft: -32 }}>
            <div className="card-body" style={{ paddingLeft: 70 }}>
              <div className="row">
                <div className="col-lg-10 col-md-12">
                  <div className="row">
                    <div className="col-lg-3 col-md-6">
                      <div className="form-group" id="form-group1">
                        <label>Tipe Driver</label>
                        <select
                          id="select-driver"
                          className="form-control"
                          onFocus={showBackdrop}
                          onBlur={hideBackdrop}
                          onChange={(e) => setDriver(e.target.value)}
                        >
                          <option>Pilih Tipe Driver</option>
                          <option value="Dengan Sopir">Dengan Sopir</option>
                          <option value="Tanpa Sopir">Tanpa Sopir</option>
                        </select>
                      </div>
                    </div>
                    <div className="col-lg-3 col-md-6">
                      <div className="form-group" id="form-group2">
                        <label>Tanggal</label>
                        <input
                          id="input-tanggal"
                          type="date"
                          className="form-control"
                          onFocus={showBackdrop}
                          onBlur={hideBackdrop}
                        />
                      </div>
                    </div>
                    <div className="col-lg-3 col-md-6">
                      <div className="form-group" id="form-group3">
                        <label>Waktu Jemput/Ambil</label>
                        <input
                          id="input-jemput"
                          type="time"
                          className="form-control"
                          onFocus={showBackdrop}
                          onBlur={hideBackdrop}
                        />
                      </div>
                    </div>
                    <div className="col-lg-3 col-md-6">
                      <div className="form-group" id="form-group4">
                        <label>Jumlah Penumpang</label>
                        <div className="input-group">
                          <input
                            id="input-penumpang"
                            type="number"
                            className="form-control border-end-0"
                            placeholder="Jumlah"
                            onFocus={showBackdrop}
                            onBlur={hideBackdrop}
                          />
                          <div className="input-group-append"></div>
                          <div className="input-group-text bg-white">
                            <img
                              src="assets/images/user-icon.svg"
                              alt="User Icon"
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-lg-2 col-md-12">
                  <div className="form-group" id="form-group5">
                    <label>&nbsp;</label>
                    <button
                      id="searchButton"
                      type="button"
                      className="btn btn-success col-sm-12"
                      onClick={onSearch}
                    >
                      Cari Mobil
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
        <div class="row">
          {listCarsJson.map((car) => {
            return <Car key={car.id}>{car.model}</Car>;
          })}
        </div>
        <div
          className="modal-backdrop background-filter"
          style={{ display: "none", backgroundColor: "rgba(0,0,0,0.5)" }}
        ></div>
      
    </>
  );
};

export default Cars;
