import { useEffect } from "react"
import Ctabanner from "../components/Ctabanner"
import FAQ from "../components/FAQ"
import Ourservice from "../components/Ourservice"
import Testimonial from "../components/Testimonial"
import Whyus from "../components/Whyus"

const Home = () => {
    useEffect(() => {
        if(window.loadOwlCarousel){
            window.loadOwlCarousel()
        }
    }, [])
    return (
        <>
        <Ourservice/>
        <Whyus/>
        <Testimonial/>
        <Ctabanner/>
        <FAQ/>

        </>
    )
}

export default Home